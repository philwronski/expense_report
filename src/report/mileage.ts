const MILEAGE_ALLOWANCE_SCALE = [
    {
        power: '3CV',
        lower: (d:number): number => d*0.456,
        medium: (d:number): number => ((d*0.273) + 915),
        highter: (d:number): number => d*0.318
    },
    {
        power: '4CV',
        lower: (d: number): number => d * 0.523,
        medium: (d: number): number => ((d * 0.294) + 1147),
        highter: (d: number): number => d * 0.352
    },
    {
        power: '5CV',
        lower: (d: number): number => d * 0.548,
        medium: (d: number): number => ((d * 0.308) + 1200),
        highter: (d: number): number => d * 0.368
    },
    {
        power: '6CV',
        lower: (d: number): number => d * 0.574,
        medium: (d: number): number => ((d * 0.323) + 1256),
        highter: (d: number): number => d * 0.386
    },
    {
        power: '7CV',
        lower: (d: number): number => d * 0.601,
        medium: (d: number): number => ((d * 0.340) + 1301),
        highter: (d: number): number => d * 0.405
    }
];

export const findAllowanceScaleByPower = (power: string) => {
    const defaultScale = MILEAGE_ALLOWANCE_SCALE[0];
    return MILEAGE_ALLOWANCE_SCALE.find(p => p.power === power) || defaultScale;
};

export const computeMileage = (power: string, miles: number) => {
    const {lower, medium, highter} = findAllowanceScaleByPower(power);
    if (miles <= 5000) {
        return lower(miles);
    } else if (miles > 5000 && miles <= 20000) {
        return medium(miles);
    } else {
        return highter(miles);
    }
};
