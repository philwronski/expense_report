export type TPrice = {
    value: number;
    currency: string;
}

export enum ExpenseCategory {
    TRANSPORT = 'Transport',
    HOTEL = 'Hébergement',
    FOOD = 'Repas',
    KILOMETER = 'Frais kilométrique',
    TELECOM = 'Internet & Téléphone'
}

export type ExpenseLine = {
    title: string;
    description?: string;
    category: ExpenseCategory;
    expenseDate: Date;
    priceWT: TPrice;
    tva: number;
}

export type CategorisedExpenses = {
    category: ExpenseCategory;
    totalWt: TPrice;
    total: TPrice;
    expenses: ExpenseLine[];
}


