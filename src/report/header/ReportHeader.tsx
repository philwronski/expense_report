import React, { FunctionComponent } from 'react';

import './report-header.scss';

type HeaderProps = {
    title: string,
    from: Date,
    to: Date
}

const ReportHeader: FunctionComponent<HeaderProps> = ({title, from, to}) => {

    const star = from.toLocaleString('fr', {year: 'numeric', month: 'numeric', day: 'numeric'});
    const end = to.toLocaleString('fr', {year: 'numeric', month: 'numeric', day: 'numeric'});

    return (
        <section className='report__header'>
            <h1>Note de frais</h1>
            <h2>{title}</h2>
            <span>Période du {star} au {end}</span>
        </section>
    );
};

export default ReportHeader;
