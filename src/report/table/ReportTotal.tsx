import React, { FunctionComponent } from 'react';

import './report-total.scss';
import Price from '../../common/price/Price';

type ReportTotalProps = {
    totalHT: number;
    totalTVA: number;
}

const ReportTotal: FunctionComponent<ReportTotalProps> = ({totalHT, totalTVA}) => {
    const total = totalHT + totalTVA;

    return (
        <table className='report__total'>
            <tbody>
                <tr>
                    <th>Total HT</th>
                    <td>
                        <Price value={totalHT}
                               currency={'€'}
                               digits='1.2'/>
                    </td>
                </tr>
                <tr>
                    <th>Total TVA</th>
                    <td>
                        <Price value={totalTVA}
                               currency={'€'}
                               digits='1.2'/>
                    </td>
                </tr>
                <tr className='report__totalTTC'>
                    <th>Total TTC</th>
                    <td>
                        <Price value={total}
                               currency={'€'}
                               digits='1.2'/>
                    </td>
                </tr>
            </tbody>
        </table>
    );
};

export default ReportTotal;
