import React, { Component, Fragment } from 'react';
import { CategorisedExpenses, ExpenseLine } from '../report.type';
import Price from '../../common/price/Price';
import { calculatePrice } from '../../utils/calculation';

import './report-table.scss';

type TableProps = {
    expenses: CategorisedExpenses[]
}

class ReportTable extends Component<TableProps> {

    constructor(props: TableProps) {
        super(props);
    }

    buildExpenseName(title: string, description?: string): string {
        if (!!description) {
            return `${title} - ${description}`;
        }

        return title;
    }

    formatDate(date: Date) {
        return date.toLocaleString('fr', {year: 'numeric', month: 'numeric', day: 'numeric'});
    }

    renderExpense(expense: ExpenseLine, index: number) {
        return (
            <tr key={index} className='expense__line'>
                <td>{this.formatDate(expense.expenseDate)}</td>
                <td>{this.buildExpenseName(expense.title, expense.description)}</td>
                <td className='align-right'>
                    <Price value={expense.priceWT.value}
                           currency={expense.priceWT.currency}
                           digits='1.2'/>
                </td>
                <td className='align-right'>{expense.tva}</td>
                <td className='align-right'>
                    <Price value={calculatePrice(expense.priceWT.value, expense.tva)}
                           currency={expense.priceWT.currency}
                           digits='1.2'/>
                </td>
            </tr>
        )
    }

    renderCategory(category: CategorisedExpenses, index: number) {
        return (
            <Fragment key={category.category}>
                <tr className='expense__category'>
                    <td className='align-center' colSpan={2}>{category.category}</td>
                    <td className='align-right'>
                        <Price value={category.totalWt.value}
                               currency={category.totalWt.currency}
                               digits='1.2' />
                    </td>
                    <td/>
                    <td className='align-right'>
                        <Price value={category.total.value}
                               currency={category.total.currency}
                               digits='1.2' />
                    </td>
                </tr>
                {
                    category.expenses.map((e, j) => this.renderExpense(e, j))
                }
                <tr className='expense__separator'><td colSpan={5}></td></tr>
            </Fragment>
        );
    }

    render() {

        const { expenses } = this.props;

        return (
            <table className='report__table'>
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Montant HT</th>
                        <th>TVA</th>
                        <th>Montant TTC</th>
                    </tr>
                </thead>
                <tbody>
                {
                    expenses.map((c, index) => this.renderCategory(c, index))
                }
                </tbody>
            </table>
        );
    }

}

export default ReportTable;
