import React, { FunctionComponent } from 'react';

import './society.scss';

type SocietyProps = {
    name: string;
    address: string;
    zipCode: string;
    city: string;
    email: string;
    phone: string;
    logo?: string;
}

const Society: FunctionComponent<SocietyProps> = ({name, address, zipCode, city, logo, email, phone}) => {
    return (
        <address className='society'>
            { logo ? <img src={logo} /> : null }
            <div>{name}</div>
            <div>{address}</div>
            <div>{zipCode} {city}</div>
            <div><a href={`mailto:${email}`}>{email}</a></div>
            <div>{phone}</div>
        </address>
    );
};

export default Society;
