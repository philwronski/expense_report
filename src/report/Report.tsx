import React, { Component } from 'react';
import ReportHeader from './header/ReportHeader';
import ReportTable from './table/ReportTable';
import ReportTotal from './table/ReportTotal';

import './report.scss';
import Society from './society/Society';

import { CategorisedExpenses, ExpenseLine } from './report.type';
import { calculatePrice, calculateTVA } from '../utils/calculation';
import { ReportContext } from '../App';

class Report extends Component {

    groupExpenses(expenses: ExpenseLine[]): CategorisedExpenses[] {
        return expenses.reduce((acc: CategorisedExpenses[], e: ExpenseLine) => {
            const category = acc.find(i => i.category === e.category);
            if(!!category) {
                category.expenses.push(e);
                category.totalWt.value += e.priceWT.value;
                category.total.value += calculatePrice(e.priceWT.value, e.tva);
            } else {
                acc.push({
                    category: e.category,
                    totalWt: {...e.priceWT},
                    total: {value: calculatePrice(e.priceWT.value, e.tva), currency: e.priceWT.currency},
                    expenses: [e]
                })
            }

            return acc;
        }, []);
    }

    calculateTotals(expenses: ExpenseLine[]): {totalHT: number, totalTVA: number} {
        return expenses.reduce((acc, e) => {
            acc.totalHT += e.priceWT.value;
            acc.totalTVA += calculateTVA(e.priceWT.value, e.tva);
            return acc;
        }, {totalHT: 0, totalTVA: 0});
    }

    render() {
        return (
          <ReportContext.Consumer>
              {({ state }) => {
                  const expenses = this.groupExpenses(state.expenses);
                  const {totalHT, totalTVA} = this.calculateTotals(state.expenses);
                  return (
                    <section className='report'>
                        {/*<Society name={'Clever Code SASU'}*/}
                        {/*         address={'7 rue du dolingen'}*/}
                        {/*         zipCode={'78440'}*/}
                        {/*         city={'Gargenville'}*/}
                        {/*         email={'philippe.wronski@clever-code.fr'}*/}
                        {/*         phone={'0770046946'}/>*/}
                        <ReportHeader
                          title={state.name}
                          from={new Date(state.start)}
                          to={new Date(state.end)}/>
                        <ReportTable expenses={expenses}/>
                        <ReportTotal totalHT={totalHT} totalTVA={totalTVA}/>
                    </section>
                  );
              }}
          </ReportContext.Consumer>
        );
    }
}

export default Report;
