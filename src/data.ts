import { ExpenseCategory } from './report/report.type';

export const reportData = [
    {
        title: 'Hotel idqkjshd',
        description: 'BestWestern',
        category: ExpenseCategory.HOTEL,
        expenseDate: new Date(),
        priceWT: {value: 88.78, currency: '€'},
        tva: 20,
    },
    {
        title: 'Resto  kjqsn',
        description: 'Napoli',
        category: ExpenseCategory.FOOD,
        expenseDate: new Date(),
        priceWT: {value: 17.60, currency: '€'},
        tva: 20,
    },
    {
        title: 'Resto  kjqsn',
        description: 'Kebab',
        category: ExpenseCategory.FOOD,
        expenseDate: new Date(),
        priceWT: {value: 13.60, currency: '€'},
        tva: 20,
    },
    {
        title: 'Taxi',
        description: 'Taxi jusqu\'a l\'aéroport',
        category: ExpenseCategory.TRANSPORT,
        expenseDate: new Date(),
        priceWT: {value: 174.00, currency: '€'},
        tva: 20,
    }
];
