import React, { Component } from 'react';
import { ExpenseCategory, ExpenseLine } from '../report/report.type';
import { ReportContext } from '../App';

type ReportFormProps = {
  title: string;
  description: string;
  category: string;
  amount: number;
  isTTC: boolean;
  tva: number;
  expenseDate: string;
}

type ReportFormState = {
  title: string;
  description: string;
  category: string;
  amount: number;
  isTTC: boolean;
  tva: number;
  expenseDate: string;
}

const initialState = {
  title: '',
  description: '',
  category: ExpenseCategory.TRANSPORT,
  amount: 0,
  isTTC: false,
  tva: 20,
  expenseDate: new Date().toISOString().split('T')[0]
};

class ReportForm extends Component<ReportFormProps, ReportFormState> {

  static defaultProps = {...initialState};

  constructor(props: ReportFormProps) {
    super(props);
    this.state = {
      title: props.title,
      description: props.description,
      category: props.category,
      amount: props.amount,
      isTTC: props.isTTC,
      tva: props.tva,
      expenseDate: props.expenseDate
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleIsTTCChange = this.handleIsTTCChange.bind(this);
    this.handleChangeContext = this.handleChangeContext.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeContext(event: any, dispatch: any) {
    const name = event.target.name;
    const value = event.target.value;
    dispatch({type: 'updateHeader', payload: {name, value}});
  }

  handleChange(event: any) {
    const name = event.target.name;
    const value = event.target.value;
    const newState = {[name]: value} as Pick<ReportFormState, keyof ReportFormState>;
    this.setState(newState);
  }

  handleIsTTCChange(event: any) {
    const value = event.target.checked;
    const newState = {isTTC: value} as Pick<ReportFormState, keyof ReportFormState>;
    this.setState(newState);
  }

  handleSubmit(event: any, dispatch: any) {
    event.preventDefault();
    const {title, description, category, amount, isTTC, tva, expenseDate} = this.state;
    const computedAmount = isTTC ? Number(amount)/(1+(tva/100)) : Number(amount);
    dispatch({type: 'addExpense', expense: {
      title,
        description,
        category,
        expenseDate: new Date(expenseDate),
        priceWT: {
        value: computedAmount,
          currency: '€'
      },
      tva,
    } as ExpenseLine});

    this.setState(initialState);
  }

  printReport() {
      // const printContents = document.querySelectorAll('.report')[0].innerHTML;
      // const originalContents = document.body.innerHTML;
      //
      // document.body.innerHTML = printContents;
      // window.print();
      // document.body.innerHTML = originalContents;

    const printContents = document.querySelectorAll('.report')[0].innerHTML;
    const styles = document.querySelectorAll('style');
    let contentStyles = '';
    styles.forEach(s => contentStyles += s.outerHTML);
    const a = window.open('', '');
    if(a) {
      a.document.write('<html>');
      a.document.write(`<head>${contentStyles}</head>`);
      a.document.write('<body>');
      a.document.write(printContents);
      a.document.write('</body></html>');
      a.print();
      a.close();
    }

  }

  render() {
    const {title, description, category, amount, isTTC, tva, expenseDate} = this.state;

    return (
      <ReportContext.Consumer>
        {({ state, dispatch }) => (
          <form onSubmit={(e) => this.handleSubmit(e, dispatch)}>
            <div className="input-group">
              <label htmlFor="name">Nom</label>
              <input type="text" name="name" value={state.name} onChange={(e) => this.handleChangeContext(e, dispatch)}/>
            </div>
            <div className="input-group">
              <label htmlFor="start">Commence le</label>
              <input type="date" name="start" value={state.start} onChange={(e) => this.handleChangeContext(e, dispatch)}/>
            </div>
            <div className="input-group">
              <label htmlFor="end">Fini le</label>
              <input type="date" name="end" value={state.end} onChange={(e) => this.handleChangeContext(e, dispatch)}/>
            </div>
            <button type="button" onClick={() => this.printReport()}>Print</button>

            <hr/>

            <div className="input-group">
              <label htmlFor="expenseDate">Date de la dépense</label>
              <input type="date" name="expenseDate" value={expenseDate} onChange={this.handleChange}/>
            </div>

            <div className="input-group">
              <label htmlFor="title">Titre</label>
              <input type="text" name="title" value={title} onChange={this.handleChange}/>
            </div>

            <div className="input-group">
              <label htmlFor="description">Description</label>
              <textarea name="description" value={description} onChange={this.handleChange}/>
            </div>

            <div className="input-group">
              <label htmlFor="category">Catégorie</label>
              <select name="category" value={category} onChange={this.handleChange}>
                {
                  Object.values(ExpenseCategory).map(c => <option key={c} value={c}>{c}</option>)
                }
              </select>
            </div>

            <div className="input-group">
              <label htmlFor="amount">Montant</label>
              <input type="number" name="amount" step="0.01" value={amount} onChange={this.handleChange}/>

              <label htmlFor="isTTC">TTC</label>
              <input type="checkbox" name="isTTC" checked={isTTC} onChange={this.handleIsTTCChange}/>
            </div>

            <div className="input-group">
              <label htmlFor="tva">TVA</label>
              <input type="number" name="tva" step="1" value={tva} onChange={this.handleChange}/>
            </div>
            <button type="submit">Ajouter</button>
          </form>
        )}
      </ReportContext.Consumer>
    )
  }

}

// ReportForm.contextType = ReportContext;

export default ReportForm;
