import React, { useContext, useReducer, useState } from 'react';

import Report from './report/Report';
import ReportForm from './form/ReportForm';

import './App.scss';
import ExpenseList from './expenses/list/ExpenseList';
import { reportData } from './data';
import { ExpenseLine } from './report/report.type';

const defaultReportContext = {
    name: 'Janvier 2020',
    start: '2020-01-01',
    end: '2020-01-31',
    expenses: [] as ExpenseLine[]
  // setReport: (x: any) => {},
  // removeExpense: (x: any):void => {}
};


function reducer(state: any, action: any) {
  switch (action.type) {
    case 'addExpense':
      return {
        ...state,
        expenses: [...state.expenses, action.expense]
      };
    case 'removeExpense':
      console.log(action, state);
      return {
        ...state,
        expenses: state.expenses.filter((e: ExpenseLine, i: number) => i !== action.id)
      };
    case 'updateHeader':
      return {
        ...state,
        [action.payload.name]: action.payload.value
      };
    case 'reset':
      return defaultReportContext;
    default:
      throw new Error();
  }
}

export const ReportContext = React.createContext<any>(defaultReportContext);

const App = () => {

  // const defaultContext = useContext(ReportContext);
  const [state, dispatch] = useReducer(reducer, defaultReportContext);
  // const [report, setReport] = useState({
  //   ...defaultContext.report
  // });

  return (
    <ReportContext.Provider value={{state, dispatch}}>
      <main className='App'>
        <div className='App__forms'>
          <div className='App__form'>
            <ReportForm/>
          </div>
          <div className='App__expense-list'>
            <ExpenseList expenses={state.expenses}/>
          </div>
        </div>
        <div className='App__report'>
          <Report/>
        </div>
      </main>
    </ReportContext.Provider>
  );
};

export default App;
