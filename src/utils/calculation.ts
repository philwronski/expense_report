export const calculatePrice = (price: number, tva: number) => {
    return price * ((tva/100) + 1);
};

export const calculateTVA = (price: number, tva: number) => {
    return price * (tva/100);
};
