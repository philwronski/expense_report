import React, { FunctionComponent } from 'react'

import './price.scss';

type PriceProps = {
    value: number;
    currency: string;
    digits?: string;
};

const Price: FunctionComponent<PriceProps> = ({ value, currency, digits = '1.2' }) => {
    const decimalDigit = Number(digits.split('.')[1]);
    const formatted = value.toLocaleString('fr-FR', {
        minimumFractionDigits: decimalDigit,
        maximumFractionDigits: decimalDigit
    }).split(',');

    const whole = formatted[0];
    const decimal = formatted[1];

    return (
        <span className='price'>
            <span className='price__whole'>{whole}</span>
            <span className='price__decimal'>.{decimal}</span>
            <span className='price__currency'>{currency}</span>
        </span>
    );
};

export default Price;
