import React, { FunctionComponent } from 'react';

import './expense-item.scss';
import { ExpenseCategory, TPrice } from '../../report/report.type';
import Price from '../../common/price/Price';
import { ReportContext } from '../../App';

type ExpenseItemProps = {
  id: number;
  title: string;
  description?: string;
  category: ExpenseCategory;
  expenseDate: Date;
  priceWT: TPrice;
  tva: number;
}

const ExpenseItem: FunctionComponent<ExpenseItemProps> = ({id, title, description, category, expenseDate, priceWT, tva}) => {

  return (
    <ReportContext.Consumer>
      {({ dispatch }) => (
        <li className='expense-item'>
          <div>
            <h4>{title}<small>{expenseDate.toLocaleString('fr')}</small></h4>
            <p>{description}</p>
          </div>
          <div>
            <Price value={priceWT.value} currency={priceWT.currency} />
            <span>{tva}%</span>
          </div>
          <div>
            <button onClick={(e) => dispatch({type: 'removeExpense', id})}>Supprimer</button>
          </div>
        </li>
      )}
    </ReportContext.Consumer>
  );
};

export default ExpenseItem;
