import React, { FunctionComponent } from 'react';

import './expense-list.scss';
import { ExpenseLine } from '../../report/report.type';
import ExpenseItem from '../item/ExpenseItem';

type ExpenseListProps = {
  expenses: ExpenseLine[]
}

const ExpenseList: FunctionComponent<ExpenseListProps> = ({expenses}) => {
  return (
    <ul className='expense-list'>
      {
        expenses.map((e, i) =>
          <ExpenseItem key={i}
                       id={i}
                       title={e.title}
                       description={e.description}
                       category={e.category}
                       expenseDate={e.expenseDate}
                       priceWT={e.priceWT}
                       tva={e.tva}/>
        )
      }
    </ul>
  );
};

export default ExpenseList;
